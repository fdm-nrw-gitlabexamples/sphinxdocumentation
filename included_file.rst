Writing documentation in Sphinx
===============================

Sphinx originates from the field of software documentation.
We will not touch on automatically generating software documentation here, but you can find information about this in the Sphinx docs.

Markup Language
---------------

Per default, Sphinx uses the `Restructured Text <https://de.wikipedia.org/wiki/ReStructuredText>`_ markup language (`Syntax Guide <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_).
You can find a nice cheat sheet explaining the syntax `here <https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst>`_.
However, Sphinx can also use markdown, for example using `MyST <https://myst-parser.readthedocs.io/en/latest/sphinx/intro.html>`_.
