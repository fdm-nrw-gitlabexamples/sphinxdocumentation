# Sphinx Documentation with GitLab Pages

This repository demonstrates how to generate an HTML documentation website from markup files and host them.
Sphinx is a documentation generator from the Python ecosystem which can be used to automatically create source code documentation from code comments.
In this case, however, we only use it to convert restructured text files to html.

To reproduce this, you need Python and [sphinx installed](https://www.sphinx-doc.org/en/master/usage/installation.html), as well as `make`.

## What happened in this repository?
This repo uses three features of gitlab: A git repository to manage the code, a pipeline to build the website, and pages to host the built website.
In the current state of [this repository](https://gitlab.com/fdm-nrw-gitlabexamples/sphinxdocumentation/-/tree/main), we already created a sphinx project, wrote some documentation, and configured our pipeline.

Every time a file is changed — i.e. every time a commit is pushed to the server — the pipeline defined in the file [`.gitlabci.yml`](https://gitlab.com/fdm-nrw-gitlabexamples/sphinxdocumentation/-/blob/main/.gitlab-ci.yml) is executed.
A (kind of) virtual machine called a GitLab runner is then started and performs the actions defined for the pipeline.
In our case, it install some dependencies, translates the documentation (by running `make`) and copies the html files from the folder `_build/html` to the folder `public`, where GitLab pages expectes them. You can see the rendered website here: https://fdm-nrw-gitlabexamples.gitlab.io/sphinxdocumentation

This (somewhat unwieldy) link is composed from:

- the name of our GitLab group: `fdm-nrw-gitlabexamples`
- the domain used by GitLab to host GitLab pages: `gitlab.io`
- and the name of this repository: `sphinxdocumentation`.

## How did we get there?
Starting from a newly created repository containing only a readme, we use `sphinx-quickstart` to create a new sphinx project.
We can find (and change) most of the information we enter here later in the file `conf.py`.

![Sphinx quickstart animated gif](_static/sphinx-quickstart.gif)

Our content can then be written in the file [`index.rst`](https://gitlab.com/fdm-nrw-gitlabexamples/sphinxdocumentation/-/edit/main/index.rst) or in additional files that are then included in `index.rst` under the `toctree` directive as we did in this repo with [`included_file`](https://gitlab.com/fdm-nrw-gitlabexamples/sphinxdocumentation/-/edit/main/index.rst#L24).


