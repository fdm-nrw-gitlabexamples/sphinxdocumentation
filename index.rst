.. Demo-Documentation documentation master file, created by
   sphinx-quickstart on Thu Jul  8 14:00:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to our Demo-Documentation!
==================================

This is a small demo on how to build a documentation with Sphinx in GitLab.
Whats happens in the background here:

- You write your documentation as restructured text (rst).
- Sphinx can translate these into html and build a website from them.
- Using a GitLab Runner (a kind of virtual machine provided by your GitLab instance)
  we automate this to always run when a new commit is made in this repository.
- Using GitLab Pages, we provide this documentation as a website.

We removed some parts of the file generated with `sphinx-quickstart` for brevity.

.. toctree::
   :maxdepth: 2
   :caption: Topics:

   included_file
